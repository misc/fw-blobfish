<?php
  namespace greenscale\server\database;

  use greenscale\server\database\DatabaseManager;
  use greenscale\server\io\log;

  /**
   * Class DatabaseView provides abstract definition for database view classes
   * Dibi is used as abstraction layer for database query handling
   * @author      Martin Springwald <springwald@greenscale.de>
   * @license     Greenscale Open Source License
   */
  abstract class DatabaseView {
    /**
     * Import Query traits
     */
    use DatabaseQuery;

    /**
     * Connection
     * @var     \DibiConnection
     */
    protected $connection;

    /**
     * Constructor of class DatabaseView intializes Database connection
     */
    public function __construct ($db_name) {
      $this->connection = DatabaseManager::get_instance($db_name)->get_connection();
    }

    /**
     * check if frame is safe
     * @param <number> $frame_from
     * @return
     */
    public function safe_frame_from ($frame_from) {
      if ($frame_from === null) {
        return 0;
      }
      return $frame_from;
    }

    /**
     * check if frame is safe
     * @param <number> $frame_to
     * @return
     */
    public function safe_frame_to ($frame_to) {
      if (is_float($frame_to) || ($frame_to == PHP_INT_MAX)) {
        throw new \Exception("UnsafeRange");
      }
      if ($frame_to === null) {
        $frame_to = PHP_INT_MAX - 1;
      }
      return $frame_to;
    }
  }
?>
