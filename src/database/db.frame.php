<?php
	namespace greenscale\server\database;
	
	/**
	 * Trait DatabaseFrame
	 * @author		Martin Springwald <springwald@greenscale.de>
	 * @license		Greenscale Open Source License
	 */
	trait DatabaseFrame {

		/**
		 * get frame from
		 * @param $data
		 * @return int|null
		 */
		public function safe_get_frame_from ($data) {
			if (isset($data->payload) && isset($data->payload->frame)) {
				return $data->payload->frame->from;
			}
			else {
				return null;
			}
		}

		/**
		 * get frame from
		 * @param $data
		 * @return int|null
		 */
		public function safe_get_frame_to ($data) {
			if (isset($data->payload) && isset($data->payload->frame)) {
				return $data->payload->frame->to;
			}
			else {
				return null;
			}
		}
	}
?>
