<?php
    namespace greenscale\server\database;

    use greenscale\server\io\Log;
    use greenscale\server\Config;

    /**
     * Trait DatabaseQuery
     * @author      Martin Springwald <springwald@greenscale.de>
     * @license     Greenscale Open Source License
     */
    trait DatabaseQuery {

        /**
         * query
         * @param \... $args
         * @return
         */
        public function query (...$args) {
            if (Config::get()->main->debug_log) {
                Log::write("-- query: " . $this->connection->translate(...$args) . "\n");
            }
            $result = $this->connection->query(...$args);
            return $result;
        }

        /**
         * multi line query
         * @param string $template
         * @param \... $args
         * @return
         */
        public function query_spread ($template, ...$args) {
            $lines = preg_split(Config::get()->main->{'query_split'}, $template);
            $results = [];
            foreach ($lines as $line) {
                array_push($results, $this->query($line, ...$args));
            }
            return $results;
        }

        /**
         * query one, return element directly or null
         * @param \... $args
         * @return
         */
        public function query_one (...$args) {
            $rowset = $this->query(...$args);
            $result = $rowset->fetchAll(NULL, 1);
            $rowset->free();
            if (count($result) === 0) {
                return null;
            }
            else {
                return reset($result);
            }
        }

        /**
         * cache query result
         * @param \... $args
         * @return
         */
        public function cache_query_result ($cachable, ...$args) {
            $do_cache = $cachable && isset(Config::get()->main->cache_store_path);
            if ($do_cache) {
                $prefix = ((count($args) > 0) ? hash('md5', serialize($args[0])) . "." : "");
                $sig = hash('md5', serialize($args));
                $cachepath = Config::get()->main->cache_store_path.DIRECTORY_SEPARATOR;
                $ftime = @filemtime($cachepath.$prefix.$sig);
                $ttl = intval(Config::get()->main->cache_store_ttl);
                if ($ftime and (time() - $ftime <= $ttl)) {
                    return unserialize(file_get_contents($cachepath.$prefix.$sig));
                }
            }
            $rowset = $this->query(...$args);
            $result = $rowset->fetchAll();
            $rowset->free();
            if ($do_cache) {
                file_put_contents($cachepath.$prefix.$sig, serialize($result));
            }
            return $result;
        }
        
        /**
         * invalide cache
         * @param $prefix
         * @return
         */
        public function invalidate_cache ($prefix) {
            if (isset(Config::get()->main->cache_store_path) && !empty($prefix)) {
                $prefix = hash('md5', serialize($prefix));
                $cachepath = Config::get()->main->cache_store_path.DIRECTORY_SEPARATOR;
                foreach(glob($cachepath.$prefix.".*") as $filename) {
                    unlink($filename);
                }
            }            
        }

        /**
         * query all
         * @param \... $args
         * @return
         */
        public function query_all (...$args) {
            return $this->cache_query_result(false, ...$args);
        }

        /**
         * query all
         * @param \... $args
         * @return
         */
        public function query_all_cached (...$args) {
            return $this->cache_query_result(true, ...$args);
        }

        /**
         * query framed
         * @param int $frame_from
         * @param int $frame_to
         * @param \... $args
         * @return
         */
        public function query_framed ($frame_from, $frame_to, ...$args) {
            $frame_from = $this->safe_frame_from($frame_from);
            $frame_to = $this->safe_frame_to($frame_to);
            array_push($args, $frame_from);
            array_push($args, $frame_to-$frame_from+1);
            return $this->query_all(...$args);
        }

        /**
         * Determine query file path by context and name
         * @param       string $name Template name
         * @param       string $context Context
         * @return  string
         */
        private function determine_query_file_path_by_name ($name, $context = null) {
            if ($context === null) {
                $context = $this->name;
            }
            $prefix = @Config::get()->main->{'queries_store_path'};
            if ($prefix == null) {
                $prefix = "queries";
            }
            $result =
                $prefix.DIRECTORY_SEPARATOR.
                $context.DIRECTORY_SEPARATOR.
                $name.'.sql';
            return $result;
        }

        /**
         * Get database names for template replacements
         * @return  array
         */
        public function populate_database_names () {
            $name_array = array();
            foreach(Config::get()->{'db_native'}  as $key => $value) {
                switch ($this->connection->getConfig('driver')) {
                    case "mysql":
                    case "mysqli":
                    case "postgre": {
                        $name_array["%DB::$key%"] = $value->database;
                        break;
                    }
                    case "sqlite":
                    case "sqlite3": {
                        $name_array["%DB::$key%"] = "main";
                        break;
                    }
                    default: {
                        throw new \Exception("Unsupported database driver");
                        break;
                    }
                }
            }
            return $name_array;
        }

        /**
         * Preprocess Template
         */
        public function preprocess_template ($content) {
            $data = $this->populate_database_names();
            switch ($this->connection->getConfig('driver')) {
                case "mysql":
                case "mysqli": {
                    $data['%IPA%'] = 'INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT';
                    $data['%KEY%'] = 'UNSIGNED';
                    $data['%ENC%'] = 'COLLATE utf8mb4_unicode_ci';
                    $data['%TXT%'] = 'CHAR CHARACTER SET utf8';
                    $data['%DTS%'] = 'DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP';
                    $data['%NOW%'] = 'CURRENT_DATE()';
                    $data['%AGGREGATE%'] = 'GROUP_CONCAT';
                    $data['%SEPARATOR%'] = 'SEPARATOR';
                    $data['%CASE%'] = 'IF(';
                    $data['%THEN%'] = ',';
                    $data['%ELSE%'] = ',';
                    $data['%END%'] = ')';
                    $data['%SEL%'] = '';
                    $data['%FKON%'] = 'SET FOREIGN_KEY_CHECKS=1';
                    $data['%FKOFF%'] = 'SET FOREIGN_KEY_CHECKS=0';
                    break;
                }
                case "sqlite":
                case "sqlite3": {
                    $data['%IPA%'] = 'INTEGER PRIMARY KEY AUTOINCREMENT';
                    $data['%KEY%'] = '';
                    $data['%ENC%'] = '';
                    $data['%TXT%'] = 'TEXT';
                    $data['%DTS%'] = 'DEFAULT CURRENT_TIMESTAMP';
                    $data['%NOW%'] = 'DATE("now")';
                    $data['%AGGREGATE%'] = 'GROUP_CONCAT';
                    $data['%SEPARATOR%'] = ',';
                    $data['%CASE%'] = 'CASE WHEN';
                    $data['%THEN%'] = 'THEN';
                    $data['%ELSE%'] = 'ELSE';
                    $data['%END%'] = 'END';
                    $data['%SEL%'] = 'SELECT * FROM';
                    $data['%FKON%'] = 'PRAGMA foreign_keys = ON';
                    $data['%FKOFF%'] = 'PRAGMA foreign_keys = OFF';
                    break;
                }
                case "postgre": {
                    $data['%IPA%'] = 'SERIAL PRIMARY KEY';
                    $data['%KEY%'] = '';
                    $data['%ENC%'] = '';
                    $data['%TXT%'] = 'TEXT';
                    $data['%DTS%'] = 'DEFAULT CURRENT_TIMESTAMP';
                    $data['%NOW%'] = 'CURRENT_DATE';
                    $data['%AGGREGATE%'] = 'string_agg';
                    $data['%SEPARATOR%'] = ',';
                    $data['%CASE%'] = 'CASE WHEN';
                    $data['%THEN%'] = 'THEN';
                    $data['%ELSE%'] = 'ELSE';
                    $data['%END%'] = 'END';
                    $data['%SEL%'] = '';
                    $data['%FKON%'] = 'SET session_replication_role = \'origin\'';
                    $data['%FKOFF%'] = 'SET session_replication_role = \'replica\'';
                    break;
                }
                default: {
                    throw new \Exception("Unsupported database driver");
                    break;
                }
            }
            $result = $content;
            foreach ($data as $key => $value) {
                $result = str_replace($key, $value, $result);
            }
            return $result;
        }

        /**
         * Get query template
         * @param       string $name Template name
         * @return  string
         */
        public function get_query_template ($name, $context = null) {
            if ($context === null) {
                $context = $this->name;
            }
            $path = $this->determine_query_file_path_by_name($name, $context);
            $template = file_get_contents($path);
            $result = $this->preprocess_template($template);
            return $result;
        }
    }
?>
