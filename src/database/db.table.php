<?php
	namespace greenscale\server\database;
	
	use greenscale\server\io\log;
	use greenscale\server\database\DatabaseManager;
	
	/**
	 * Class DatabaseTable abstracts database table interface
	 * @author 		Martin Springwald <springwald@greenscale.de>
	 * @license		Greenscale Open Source License
	 */
	/*abstract*/ class DatabaseTable {
		/**
		 * Import Query traits
		 */
		use DatabaseQuery;
		
		/**
		 * Name of table
		 * @var			string
		 */
		protected $name;
		
		/**
		 * Connection
		 * @var			\DibiConnection
		 */
		protected $connection;		
		
		/**
		 * Constructor of class DatabaseTable initializes Database connection
		 */
		public function __construct ($name, $db_name) {
			$this->name = $name;
			$this->connection = DatabaseManager::get_instance($db_name)->get_connection();
		}
		
		/**
		 * begin
		 * @return  
		 */
		public function begin () {
			$this->connection->begin();
		}
		
		/**
		 * commit
		 * @return  
		 */
		public function commit () {
			$this->connection->commit();
		}		
		
		/**
		 * get last id
		 * @return  
		 */
		public function getlastid () {
			if ($this->connection->getAffectedRows() > 0) {
				return $this->connection->getInsertId();
			}
			return false;
		}
		
		/**
		 * get affected rows
		 * @return  
		 */
		public function getaffectedrows () {
			return $this->connection->getAffectedRows();
		}

		/**
		 * Make insertable array from object
		 * @param 		object $obj Object containing data
		 * @return		array
		 * @todo make static
		 */
		public function makeTransferObject($obj) {
			return (array) $obj;
		}
		
		/**
		 * Create scheme
		 * @return		\dibi\Result
		 */
		public function createScheme() {
			$query_template = $this->get_query_template('create');
			return $this->connection->query($query_template);
		}
		
		/**
		 * Drop table
		 * @return		\dibi\Result
		 */
		public function dropTable () {
			$query_template = $this->get_query_template('drop');
			return $this->connection->query($query_template);
		}
		
		/**
		 * Clear table
		 * @return		\dibi\Result
		 */
		public function clearTable () {
			$query_template = $this->get_query_template('clear');
			return $this->connection->query($query_template);
		}
	}
?>
