<?php
    namespace greenscale\server\database;

    use dibi\dibi;
    use greenscale\server\Config;

    /**
     * Class DatabaseManager provides connection pooling to the database
     * Dibi is used as abstraction layer for database query handling
     * @author          Martin Springwald <springwald@greenscale.de>
     * @license         Greenscale Open Source License
     */
    class DatabaseManager {
        /**
         * Connection
         * @var         \DibiConnection
         */
        static private $connection = array();

        /**
         * Database name
         * @var         string
         */
        private $db_name;

        /**
         * Constructor of class DatabaseManager reads profile from config and establishes connection
         */
        function __construct ($db_name) {
            $this->db_name = $db_name;
            if (!isset(self::$connection[$db_name])) {
                $profile = (array) Config::get()->db_native->{$db_name};
                $this->set_connection(new \DibiConnection($profile));
                if (isset($profile->onConstruct)) {
                  foreach($profile->onConstruct as $query) {
                    self::$connection[$db_name]->query($query);
                  }
                }
            }
        }

        /**
         *  Destructor
         */
        function __destruct () {
            if (isset(self::$connection[$this->db_name])) {
                if (self::$connection[$this->db_name]->isConnected()) {
                    self::$connection[$this->db_name]->disconnect();
                }
            }
        }

        /**
         * Getter for connection attribute
         * @return      \DibiConnection
         */
        public function get_connection () {
            return self::$connection[$this->db_name];
        }

        /**
         * Setter for connection attribute
         * @param       \DibiConnection $connection Connection
         */
        public function set_connection ($connection) {
            self::$connection[$this->db_name] = $connection;
        }

        /**
         * Get instance
         * @return      \DatabaseManager
         */
        public static function get_instance ($db_name) {
            return new DatabaseManager($db_name);
        }
    }
?>
