<?php
	namespace greenscale\server\auth;

	/**
	 * Class AuthSessionHandler provides session handling
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class AuthSessionHandler extends \SessionHandler {
		/**
		 * Path to session storage
		 * @var			string
		 */
		private $storage_path = null;
		
		/**
		 * Resolve path to session file
		 * @param 		string $session_id ID of session
		 * @return		string
		 */
		private function get_path_by_sess_id ($session_id) {
			$file_path = $this->storage_path.DIRECTORY_SEPARATOR."session_$session_id";
			return $file_path;
		}
		
		/**
		 * Handle open event
		 * @param		string $file_path Path to session storage
		 * @param		string $session_id ID of session
		 * @return	true
		 */
		public function open ($file_path, $session_id) {
			$this->storage_path = $file_path;
			return true;
		}
		
		/**
		 * Handle close event
		 * @return		true
		 */
		public function close () {
			return true;
		}
		
		/**
		 * Handle read event
		 * @param		string $session_id ID of session
		 * @return		string
		 */
		public function read ($session_id) {
			$file_path = $this->get_path_by_sess_id($session_id);
			return (string) @file_get_contents($file_path);
		}
		
		/**
		 * Handle write event
		 * @param		string $session_id ID of session
		 * @param		string $session_data Session data
		 * @return		boolean
		 */
		public function write ($session_id, $session_data) {
			$file_path = $this->get_path_by_sess_id($session_id);
			if ($fp = @fopen($file_path, "w")) {
				$return = fwrite($fp, $session_data);
				fclose($fp);
				if ($return === false) {
					return false;
				}
				else {
					return true;
				}
			} else {
				return false;
			}
		}
		
		/**
		 * Handle destroy event
		 * @param		string $session_id ID of session
		 * @return		boolean
		 */
		public function destroy ($session_id) {
			$file_path = $this->get_path_by_sess_id($session_id);
			return @unlink($file_path);
		}
		
		/**
		 * Handle garbage collector event
		 * @param		number $ttl
		 * @return		true
		 */
		public function gc ($ttl) {
			$rdi = new \RecursiveDirectoryIterator($this->storage_path);
			$rii = new \RecursiveIteratorIterator($rdi);
			$ri = new \RegexIterator($rii, "/^.+\/session_.+$/", \RecursiveRegexIterator::GET_MATCH);
			foreach($ri as $file) {
				$path = $file[0];
				clearstatcache(true, $path);
				if ((filemtime($path) + $ttl) < time())
					@unlink($path);
			}
			return true;
		}
	}
?>
