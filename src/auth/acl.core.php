<?php
	namespace greenscale\server\auth;

	/**
	 * Class ACLManager manages access control
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class ACLManager {
		private static $activation_plugins = array();
		private static $authenticator_plugins = array();
		private static $check_plugins = array();
		private static $login_plugins = array();
		private static $registration_plugins = array();
	
		/**
		 * Constructor of class ACLManager initializes rules and APIs
		 */		
		private function __construct () {
		}
		
		/**
		 * Check if user has sufficient access rights
		 * @param 		string $service_name
		 * @param 		object $input_data
		 * @param			string $override_user
		 * @return  	boolean
		 */
		public static function check ($service_name, $input_data = null, $override_user = null) {
			$granted = true;
			foreach(self::$check_plugins as $plugin) {
				$granted = call_user_func($plugin, $service_name, $input_data, $override_user, $granted);
			}
			return $granted;
		}
		
		/**
		 * Determine current user
		 * @return		object
		 */
		public static function get_current_user () {
			$result = (object) array();
			foreach(self::$authenticator_plugins as $plugin) {
				$result = call_user_func($plugin, $result);
			}
			return $result;
		}
		
		/**
		 * Register user
		 * @return 		object
		 */
		public static function register ($input) {
			$result = (object) array();
			if ($input != null) {
				$result = $input;
			}
			foreach(self::$registration_plugins as $plugin) {
				$result = call_user_func($plugin, $result);
			}
			return $result;
		}
		
		/**
		 * Activate user
		 * @return		object
		 */
		public static function activate ($input) {
			$result = (object) array();
			if ($input != null) {
				$result = $input;
			}
			foreach(self::$activation_plugins as $plugin) {
				$result = call_user_func($plugin, $result);
			}
			return $result;
		}
		
		/**
		 * Activate user
		 * @return		object
		 */
		public static function login ($input) {
			$result = (object) array();
			if ($input != null) {
				$result = $input;
			}
			foreach(self::$login_plugins as $plugin) {
				$result = call_user_func($plugin, $result);
			}
			return $result;
		}

		/**
		 * Register plugin
		 * @param		function $plugin
		 */
		public static function register_authenticator_plugin ($plugin) {
			array_push(self::$authenticator_plugins, $plugin);
		}
		
		/**
		 * Register plugin
		 * @param		function $plugin
		 */
		public static function register_registration_plugin ($plugin) {
			array_push(self::$registration_plugins, $plugin);
		}

		/**
		 * Register plugin
		 * @param		function $plugin
		 */
		public static function register_activation_plugin ($plugin) {
			array_push(self::$activation_plugins, $plugin);
		}

		/**
		 * Register plugin
		 * @param		function $plugin
		 */
		public static function register_check_plugin ($plugin) {
			array_push(self::$check_plugins, $plugin);
		}

		/**
		 * Register plugin
		 * @param		function $plugin
		 */
		public static function register_login_plugin ($plugin) {
			array_push(self::$login_plugins, $plugin);
		}

		/**
		 * Get instance
		 * @return	\ACLManager
		 */
		public static function get_instance () {
			return new ACLManager();
		}
	}
?>
