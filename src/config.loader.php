<?php
	namespace greenscale\server;
	
	/**
	* Generic configuration loader
	* @access		public
	* @author		Christian Fraß <frass@greenscale.de>
	* @author		Martin Springwald <springwald@greenscale.de>
	* @license		Greenscale Open Source License
	*/
	class Config {
		/**
		 * Default configuration file path
		 * @access	public
		 */
		const DEFAULT_CONF_FILE = 'conf.json';
		
		/**
		 * Name of global variable to overwrite default config file path
		 * @access	public
		 */
		const GLOB_CONF = 'conf';
		
		/**
		 * Loaded and cached config data as object
		 * @access	private
		 */
		private static $data = null;
		
		/**
		 * Get configuration as object from JSON file
		 * @access	private
		 * @static
		 */
		private static function retrieve() {
			$file = isset($GLOBALS[self::GLOB_CONF]) ?
				$GLOBALS[self::GLOB_CONF] : self::DEFAULT_CONF_FILE;
			return json_decode(file_get_contents($file));
		}
		
		/**
		 * Get config data as object from cache or file
		 * @access	public
		 * @static
		 * @return	object
		 */
		public static function get() {
			if (self::$data == null) {
				self::$data = self::retrieve();
			}
			return self::$data;
		}
		
		/**
		 * Invalidate config data cache (e.g. in reaction to changes)
		 * @access	public
		 * @static
		 */
		public static function invalidate() {
			$self::$data = null;
		}
	}
?>
