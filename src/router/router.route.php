<?php
	namespace greenscale\server\router;
	
	/**
	 * Class Route represents the association of method and path to some callback
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	*/
	class Route {
		/**
		 * HTTP method such as GET or POST
		 * @var			string
		 */
		private $method = null;
		
		/**
		 * Virtual path to the desired service relative to the entry script
		 * @var			string
		 */
		private $path = null;

		/**
		 * Associated callback
		 * @var			function
		 */
		private $callback = null;
		
		/**
		 * Constructor of class Route
		 * @param 		string $method HTTP method such as GET or POST
		 * @param		string $path Virtual path to the desired service reative to the entry script
		 * @param		function $callback Associated callback
		 */
		function __construct ($method, $path, $callback) {
			$this->set_method($method);
			$this->set_path($path);
			$this->set_callback($callback);
		}
		
		/**
		 * Setter for method attribute
		 * @param		string $method HTTP method such as GET or POST
		 */
		public function set_method ($method) {
			$this->method = $method;
		}
		
		/**
		 * Setter for path attribute
		 * @param		string $path Virtual path to the desired service reative to the entry script
		 */
		public function set_path ($path) {
			$this->path = $path;
		}

		/**
		 * Setter for callback attribute
		 * @param		function $callback Associated callback
		 */
		public function set_callback ($callback) {
			$this->callback = $callback;
		}

		/**
		 * Getter for method attribute
		 * @return		string
		 */
		public function get_method () {
			return $this->method;
		}

		/**
		 * Getter for path attribute
		 * @return		string
		 */
		public function get_path () {
			return $this->path;
		}

		/**
		 * Getter for callback attribute
		 * @return		function
		 */
		public function get_callback () {
			return $this->callback;
		}
		
		/**
		 * Execute associated callback
		 * @retrun		void
		 */
		public function execute () {
			$callback = $this->callback;
			$callback();
		}
	}
?>