<?php
	namespace greenscale\server\router;

    use greenscale\server\io\Input;
	use greenscale\server\router\Options;

	/**
	 * Class Router provides basic routing of HTTP and REST requests
	 * Requires REQUEST_METHOD and PATH_INFO to be passed through
	 * @author				Martin Springwald <springwald@greenscale.de>
	 * @license				Greenscale Open Source License
	 */
	class Router {
			/**
			 * Array of routes
			 * @var			array
			 */
			private $routes = [];
			
			/**
			 * Callback for default route
			 * @var			function
			 */
			private $default_handler = null;
			
			/**
			 * Add new route
			 * @param		function $route Route definition
			 * @param		boolean $auto_options Automatically add OPTIONS handler, optional and defaults to true
			 * @return		void
			 */
			public function add ($route, $auto_options = true) {
				if ($auto_options === true) {
					Options::install_route($this, $route->get_method(), $route->get_path());
				}
				array_push($this->routes, $route);
			}
			
			/**
			 * Set callback for default route
			 * @param 		function $default_handler Callback for default route
			 * @return		void
			 */
			public function set_default_handler ($default_handler) {
				$this->default_handler = $default_handler;
			}
				
			/**
			 * Find callback by method and path
			 * @param 		string $method HTTP method such as GET or POST
			 * @param		 	string $path Virtual path to the desired service relative to the entry script
			 * @return		false|function
			 */
			private function find ($method, $path) {
				// Iterate routes
				foreach($this->routes as $value) {
					// Match method with equation
					$method_match = ($value->get_method() == $method);
					// Match path with regex
					$path_match = (preg_match('/'.preg_quote($value->get_path(), '/').'/', $path) === 1);
					if ($method_match && $path_match) {
						// Return callback if method and path do match
						return $value;
					}
				}
				// Return false if no match found
				return false;
			}
			
			/**
			 * Match route and execute associated callback or default handler
			 * @param		string $method Method, optional
			 * @param		string $path Path, optional
			 * @return	void
			 */
			public function match ($method = null, $path = null) {
				// Gather method and path from server globals or specified parameters
				if ($method === null) {
					$method = Input::get_method();
				}
				if ($path === null) {
					$path = Input::get_path();
				}
				// Find associated callback
				$route = $this->find($method, $path);
				if ($route !== false) {
					// Execute associated callback
					$route->execute();
				}
				else {
					// Execute default callback
					$callback = $this->default_handler;
					$callback();
				}
			}
	}
?>
