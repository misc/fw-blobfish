<?php
	namespace greenscale\server\router;
	
	use greenscale\server\Config;
	use greenscale\server\io\Output;
	use greenscale\server\io\OutputSuccess;
	use greenscale\server\router\Route;
	
	/**
	 * Class Options abstracts OPTIONS requests
	 * @author				Martin Springwald <springwald@greenscale.de>
	 * @license				Greenscale Open Source License
	 */
	class Options {
		/**
		 * Method map
		 * @static
		 * @var					array
		 */
		private static $method_map = array();
		
		/**
		 * Install route
		 * @param				\Router $router Router
		 * @param				string $method Method
		 * @param				string $path Path
		 */
		public static function install_route ($router, $method, $path) {
			if (isset(self::$method_map[$path])) {
				array_push(self::$method_map[$path], $method);
			}
			else {
				self::$method_map[$path] = [$method];
			}
			self::$method_map[$path] = array_unique(self::$method_map[$path]);
			$router->add(new Route("OPTIONS", $path, function () use (&$path) {
				self::output(implode(',', self::$method_map[$path]));
			}), false);
		}
		
		/**
		 * Output answer for OPTIONS request
		 * @param				array $methods List of allowed methods
		 */
		public static function output ($methods) {
			$answer = new OutputSuccess((object) array(
				'origin' => Config::get()->main->allow_origin,
				'methods' => $methods,
				'headers' => 'origin, content-type'
			));
			Output::output($answer);
		}
	}
?>
