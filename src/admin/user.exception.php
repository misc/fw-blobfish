<?php
	namespace greenscale\server\admin;

	use greenscale\server\service\ServiceException;

	/**
	 * Class UserAdminException provides exception definition
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class UserAdminException extends ServiceException {
		public function __construct ($name) {
			parent::__construct('UserAdminController', $name);
		}
	}
?>
