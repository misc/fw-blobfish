<?php
	namespace greenscale\server\io;
	
	use greenscale\server\io\OutputAnswer;
	
	/**
	 * Class OutputSuccess models Generic answer for successful operations
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class OutputSuccess extends OutputAnswer {
		/**
		 * Options
		 * @var			object
		 */
		private $options = null;

		/**
		 * Payload
		 * @var			object
		 */
		private $payload = null;
		
		/**
		 * Payload
		 * @var			object
		 */
		private $default_content_type = null;

		/**
		 * Constructor of OutputAnswer-type class initializes options
		 * @param			object $options Options, optional
		 * @param			object $payload Payload, optional
		 */
		function __construct($options = null, $payload = null, $default_content_type = null) {
			$this->options = $options;
			$this->payload = $payload;
			$this->default_content_type = $default_content_type;
		}
		
		/**
		 * Getter for payload
		 * @return		object
		 */
		public function getPayload() {
			return $this->payload;
		}
		
		/**
		 * Generate REST answer with corresponding response code
		 * @return 		object
		 */
		public function to_rest() {
			if ($this->default_content_type === null) {
				return $this->to_rest_raw(200, $this->options, $this->payload);
			}
			else {
				return $this->to_rest_raw(200, $this->options, $this->payload, $this->default_content_type);
			}
		}
	}
?>
