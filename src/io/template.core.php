<?php
    namespace greenscale\server\io;

    use greenscale\server\Config;
    use greenscale\server\io\Log;

    /**
     * Class Template   processes templates
     * @author              Martin Springwald <springwald@greenscale.de>
     * @license             Greenscale Open Source License
     */
    class Template {
        /**
         * Add common var base uri
         * @param           object $data
         */
        private function add_common_var_base_uri ($data) {
            $data->{'base_uri'} = str_replace("%server_name%", $_SERVER['SERVER_NAME'], Config::get()->main->base_uri);
        }

        /**
         * Add common var base profile
         * @param           object $data
         */
        private function add_common_var_base_profile ($data) {
            if (Config::get()->main->base_profile != null) {
                $data->{'base_profile'} = '?profile=' . Config::get()->main->base_profile;
            }
            else {
                $data->{'base_profile'} = '';
            }
        }

        /**
         * Add common var base media
         * @param           object $data
         */
        private function add_common_var_media ($data) {
            $media_path = Config::get()->main->template_store_path.DIRECTORY_SEPARATOR."media";
            $rdi = new \RecursiveDirectoryIterator($media_path);
            $rii = new \RecursiveIteratorIterator($rdi);
            $ri = new \RegexIterator($rii, "/^.+\/([^\/]+\.dataurl)$/", \RecursiveRegexIterator::GET_MATCH);
            foreach($ri as $file) {
                $data->{$file[1]} = file_get_contents($file[0]);
            }
        }

        /**
         * Add common vars
         * @param           object $data
         */
        private function add_common_vars ($data) {
            $this->add_common_var_base_uri($data);
            $this->add_common_var_base_profile($data);
            $this->add_common_var_media($data);
        }

		private static $localization_cache = null;

        /**
         * Translate
         * @param           string $result
         * @return          string
         */
		public static function translate ($result) {
			if (!empty($result)) {
                $language = Config::get()->main->language;
                if (isset($_SESSION) && isset($_SESSION['lang'])) {
                    $language = $_SESSION['lang'];
                }
	            if (self::$localization_cache == null) {
                 	self::$localization_cache = json_decode(file_get_contents(
                        Config::get()->main->template_store_path.
                        DIRECTORY_SEPARATOR.
                        "localization".
                        DIRECTORY_SEPARATOR.
                        $language.
                        ".loc.json"
				    ));
				}
                $pattern = '/\$translate:([a-z0-9\._]+)/';
                $result = preg_replace_callback($pattern, function($matches) {
                   return self::$localization_cache->tree->{$matches[1]};
                }, $result);
                return $result;
		    }
        }

        /**
         * Generate list
         * @param       string $result
         * @return          string
         */
        public static function autolist ($result) {
            $pattern = '/\$autolist:([a-z0-9\._]+)/';
            $result = preg_replace_callback($pattern, function($matches) {
                $domain = $matches[1];
                return Template::load_html_template_from_file("list_$domain");
            }, $result);
            return $result;
        }

        /**
         * Generate form
         * @param       string $result
         * @return          string
         */
        public static function autoform ($result) {
            $pattern = '/\$autoform:([a-z0-9\._]+)/';
            $result = preg_replace_callback($pattern, function($matches) {
                $domain = $matches[1];
                return Template::load_html_template_from_file("form_$domain");
            }, $result);
            return $result;
        }

        /**
         * Generate nav
         * @param       string $result
         * @return          string
         */
        public static function autonav ($result) {
            $pattern = '/\$autonav:([a-z0-9\._]+)/';
            $result = preg_replace_callback($pattern, function($matches) {
                $domain = $matches[1];
                return Template::load_html_template_from_file("nav_$domain");
            }, $result);
            return $result;
        }

        /**
         * Process template and apply data
         * @param           $content Template content
         * @param           $data Data to apply
         * @return          string
         */
        public function process ($content, $data) {
            $result = $content;
            // strings
            {
                $result = self::autoform($result);
                $result = self::autolist($result);
                $result = self::autonav($result);
                $result = self::translate($result);
            }
            // data
            {
                $this->add_common_vars($data);
                foreach ($data as $key => $value) {
                    if (is_scalar($value)) {
                        $result = str_replace('$' . $key, strval($value), $result);
                    }
                    else {
                        Log::write(sprintf("Could not replace variable with key '%s' in template bencause value to insert is not a scalar", $key));
                    }
                }
            }
            return $result;
        }

        /**
         * Load contents from file
         * @param           $name Name of template
         */
        public static function load_template_from_file ($name) {
            return file_get_contents($name);
        }

        /**
         * Load contents from file
         * @param           $name Name of template
         */
        public static function load_html_template_from_file ($name) {
            return self::load_template_from_file(
                Config::get()->main->template_store_path.DIRECTORY_SEPARATOR
                . $name
                . ".html.tpl"
            );
        }

        /**
         * Load contents from file
         * @param           $name Name of template
         */
        public static function load_plain_template_from_file ($name) {
            return self::load_template_from_file(
                Config::get()->main->template_store_path.DIRECTORY_SEPARATOR
                . $name
                . ".plain.tpl"
            );
        }
    }
?>
