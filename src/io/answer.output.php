<?php
	namespace greenscale\server\io;
	
	/**
	 * Class OutputAnswer models Generic abstraction for answers
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	abstract class OutputAnswer {
		/**
		 * Generate raw REST anwer
		 * @param 	int $code 
		 * @param 	object $allow, optional
		 * @param 	object $payload, optional
		 * @return  object
		 */
		public function to_rest_raw (
			$code,
			$allow = null,
			$payload = null,
			$default_content_type = 'text/plain'
		) {
			$content_type = 'application/json';
			if ($payload !== null) {
				$body = json_encode($payload);
			}
			else {
				$content_type = $default_content_type;
				$body = null;
			}
			$answer = (object) array(
				'allow' => $allow,
				'content_type' => $content_type,
				'code' => $code,
				'body' => $body
			);
			return $answer;
		}
	}
?>