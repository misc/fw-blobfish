<?php
    namespace greenscale\server\io;

    use greenscale\server\admin\UserAdminException;
    use greenscale\server\Config;
    use greenscale\server\io\Log;

    /**
     * Class Upload provides upload functionality
     * @author          Martin Springwald <springwald@greenscale.de>
     * @license         Greenscale Open Source License
     */
    class Upload {
        public static function get_prefix () {
            return Config::get()->main->data_save_path.'/';
        }

        public static function get_prefix_dist () {
            return Config::get()->main->distribution_path.'/';
        }

        public static function get_random_hex () {
            $strong = false;
            $rnd = bin2hex(openssl_random_pseudo_bytes(32, $strong));
            if (!$strong) {
                throw new UserAdminException("NotEnoughEntropy");
            }
            return $rnd;
        }

        // TODO: associate ticket id with session
        public static function create_random_file () {
            $prefix = self::get_prefix();
            $choosen_name = null;
            for ($i = 0; $i < 10; $i++) {
                $random_name = self::get_random_hex();
                if (file_exists($prefix.$random_name)) {
                    continue;
                }
                else {
                    $choosen_name = $random_name;
                    $fn = fopen($prefix.$random_name, "w");
                    if ($fn === false) {
                        throw new UserAdminException("CouldNotCreateFile");
                    }
                    fclose($fn);
                    break;
                }
            }
            return $choosen_name;
        }

        private static function check_ticket ($ticket_id) {
            if ($ticket_id === null) {
                throw new UserAdminException("InvalidTicketId");
            }
        }

        // TODO: associate ticket id with session
        public static function create_random_link ($ticket_id, $suffix = "") {
            self::check_ticket($ticket_id);
            $prefix = self::get_prefix();
            $prefix_dist = self::get_prefix_dist();
            $choosen_name = null;
            for ($i = 0; $i < 10; $i++) {
                $random_name = self::get_random_hex().$suffix;
                if (file_exists($prefix.$random_name)) {
                    continue;
                }
                else {
                    $choosen_name = $random_name;
                    $fn = @symlink($prefix.$ticket_id, $prefix_dist.$random_name);
                    if ($fn === false) {
                        throw new UserAdminException("CouldNotCreateFile");
                    }
                    break;
                }
            }
            return $choosen_name;
        }

        public static function create_perma_link ($ticket_id, $suffix = "") {
            self::check_ticket($ticket_id);
            $prefix = self::get_prefix();
            $prefix_dist = self::get_prefix_dist();
            $choosen_name = $ticket_id.$suffix;
            if (!file_exists($prefix_dist.$choosen_name)) {
                $fn = @symlink($prefix.$ticket_id, $prefix_dist.$choosen_name);
                if ($fn === false) {
                    throw new UserAdminException("CouldNotCreateFile");
                }
            }
            return $choosen_name;
        }

        public static function get_max_size($size_string = null) {
            if ($size_string === null) {
                $size_string = ini_get("post_max_size");
            }
            $size_string = trim($size_string);
            $modifier = strtolower($size_string[strlen($size_string)-1]);
            $size_number = intval($size_string);
            switch($modifier) {
                case 'g':   $size_number *= 0x40000000; break; // 1024*1024*1024
                case 'm':   $size_number *= 0x100000; break; // 1024*1024
                case 'k':   $size_number *= 0x0400; break; // 1024
                default: break;
            }
            return $size_number - 256;
        }

        public static function check_ticket_in_session ($ticket_id) {
            if (isset($_SESSION["upload_tickets"])) {
                if (in_array($ticket_id, $_SESSION["upload_tickets"])) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        public static function save_ticket_to_session ($ticket_id) {
            if (isset($_SESSION["upload_tickets"])) {
                array_push($_SESSION["upload_tickets"], $ticket_id);
            }
            else {
                $_SESSION["upload_tickets"] = array($ticket_id);
            }
        }

        public static function create_ticket () {
            $ticket_id = self::create_random_file();
            $result = $ticket_id;
            self::save_ticket_to_session($ticket_id);
            if ($result === null) {
                throw new UserAdminException("CouldNotCreateTicket");
            }
            return $result;
        }

        public static function begin_upload () {
            return self::create_ticket();
        }

        public static function sanity_check ($str) {
            return !preg_match('/[^A-Za-z0-9$]/', $str);
        }

        public static function decode_slice ($base64_slice) {
            $result = base64_decode($base64_slice, true);
            if ($result === false) {
                throw new UserAdminException("InvalidInputString");
            }
            return base64_decode($base64_slice, true);
        }

        public static function continue_upload ($ticket_id, $slice_data, $slice_index) {
            $prefix = self::get_prefix();
            $slice_index = strval(intval($slice_index));
            if (self::sanity_check($ticket_id) && self::check_ticket_in_session($ticket_id)) {
                if (file_exists($prefix.$ticket_id)) {
                    $result = file_put_contents($prefix.$ticket_id.'.'.$slice_index, $slice_data);
                    if ($result === false) {
                        throw new UserAdminException("CouldNotWriteFile");
                    }
                    else {
                        return $result;
                    }
                }
            }
            throw new UserAdminException("InvalidTicketId");
        }

        public static function finish_upload ($ticket_id) {
            $prefix = self::get_prefix();
            if (self::sanity_check($ticket_id) && self::check_ticket_in_session($ticket_id)) {
                if (file_exists($prefix.$ticket_id)) {
                    $files = glob($prefix.$ticket_id.'.*');
                    $cnt = count($files);
                    $bytes_written = 0;
                    $is_broken = false;
                    for ($i = 0; $i < $cnt; $i++) {
                        $filename = $prefix.$ticket_id.'.'.strval($i);
                        if (file_exists($filename)) {
                            if (!$is_broken) {
                                $slice_data = file_get_contents($filename);
                                if ($slice_data === null) {
                                    throw new UserAdminException("CouldNotReadFile");
                                }
                                $result_write = file_put_contents($prefix.$ticket_id, $slice_data, FILE_APPEND);
                                if ($result_write === null) {
                                    throw new UserAdminException("CouldNotWriteFile");
                                }
                                $bytes_written += $result_write;
                            }
                            $result_remove = unlink($filename);
                            if ($result_remove === null) {
                                throw new UserAdminException("CouldNotRemoveFile");
                            }
                        }
                        else {
                            $is_broken = true;
                        }
                    }
                    if ($is_broken) {
                        $result_remove = unlink($prefix.$ticket_id);
                        if ($result_remove === null) {
                            throw new UserAdminException("CouldNotRemoveFile");
                        }
                        throw new UserAdminException("MissingPart");
                    }
                    else {
                        return $bytes_written;
                    }
                }
            }
            throw new UserAdminException("InvalidTicketId");
        }

        public static function get_file ($ticket_id) {
            self::check_ticket($ticket_id);
            $prefix = self::get_prefix();
            $result = file_get_contents($prefix.$ticket_id);
            if ($result === false) {
                throw new UserAdminException("CouldNotReadFile");
            }
            return $result;
        }

        public static function get_file_size ($ticket_id) {
            $prefix = self::get_prefix();
            $result = filesize($prefix.$ticket_id);
            if ($result === false) {
                throw new UserAdminException("CouldNotReadFile");
            }
            return $result;
        }

        public static function pass_file ($ticket_id) {
            self::check_ticket($ticket_id);
            $prefix = self::get_prefix();
            $result = readfile($prefix.$ticket_id);
            if ($result === false) {
                throw new UserAdminException("CouldNotReadFile");
            }
        }
    }
?>
