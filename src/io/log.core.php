<?php
	namespace greenscale\server\io;
	
	use greenscale\server\Config;
	
	/**
	 * Class Log abstracts Logger
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class Log {
		/**
		 * Write to log
		 * @static
		 * @param string $message Message to log
		 * @param bool $force_log
		 */
		public static function write($message, $force_log = false) {
			if (Config::get()->main->debug_log || $force_log) {
    			$prefix = ("[" . date("Y-m-d H:i:s") . "]");
		    	$output = ("\n" . $prefix . " " . $message . PHP_EOL);
    			file_put_contents(Config::get()->main->io_log_path, $output, FILE_APPEND);
			}
		}
	}
?>
