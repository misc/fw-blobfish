<?php
    namespace greenscale\server\io;

    use greenscale\server\io\Log;

    /**
     * Output handler
     * @author          Martin Springwald <springwald@greenscale.de>
     * @license         Greenscale Open Source License
     */
    class Output {
        /**
         * Make JSON output for REST request
         * @static
         * @param       \OutputAnswer $answer Answer object
         */
        public static function as_rest ($answer) {
            $rest_answer = $answer->to_rest();
            Log::write(sprintf("<output>\n\t%s / %s\n</output>", $rest_answer->code, $rest_answer->body));
            if (isset($rest_answer->allow)) {
                if (isset($rest_answer->allow->origin)) {
                    header("Access-Control-Allow-Origin: " . $rest_answer->allow->origin);
                }
                if (isset($rest_answer->allow->methods)) {
                    header("Access-Control-Allow-Methods: " . $rest_answer->allow->methods);
                }
                if (isset($rest_answer->allow->headers)) {
                    header("Access-Control-Allow-Headers: " . $rest_answer->allow->headers);
                }
                header("Access-Control-Allow-Credentials: true");
            }
            if (isset($rest_answer->content_type) && ($rest_answer->content_type != null)) {
                header("Content-Type: " . $rest_answer->content_type);
            }
            if (isset($rest_answer->body) && ($rest_answer->body !== null)) {
                echo $rest_answer->body;
            }
            http_response_code($rest_answer->code);
        }

        /**
         * Make output
         * @static
         * @param       \OutputAnswer $answer Answer object
         */
        public static function output ($answer) {
            self::as_rest($answer);
        }
    }
?>
