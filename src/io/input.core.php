<?php
  namespace greenscale\server\io;

  use greenscale\server\io\Log;

  /**
   * Class Input models Generic input class, mostly static
   * @author      Martin Springwald <springwald@greenscale.de>
   * @license     Greenscale Open Source License
   */
  class Input {
    /**
     * Input URI
     * @static
     * @var     string
     */
    public static $inuri = 'php://input';

    /**
     * Get method from request
     * @static
     * @return  string
     */
    public static function get_method () {
      if (isset($_SERVER['REQUEST_METHOD'])) {
        return $_SERVER['REQUEST_METHOD'];
      }
      else {
        return null;
      }
    }

    /**
     * Get path info from request
     * @static
     * @return  string
     */
    public static function get_path () {
      if (isset($_SERVER['PATH_INFO'])) {
        return $_SERVER['PATH_INFO'];
      }
      else {
        return null;
      }
    }

    /**
     * Get query string from request
     * @static
     * @return  string
     */
    public static function get_query () {
      if (isset($_SERVER['QUERY_STRING'])) {
        return $_SERVER['QUERY_STRING'];
      }
      else {
        return null;
      }
    }

    /**
     * Get content type from request
     * @static
     * @return  string
     */
    private static function get_content_type () {
      return $_SERVER['CONTENT_TYPE'];
    }

    /**
     * Check if content type is present
     * @static
     * @param   string $content_type Content type
     * @param   string $needles Search string in content type
     */
    private static function check_content_type ($content_type, $needles) {
      $result = false;
      foreach ($needles as $needle) {
        $result = ($result || (strpos($content_type, $needle) !== false));
      }
      return $result;
    }

    /**
     * Get JSON input from REST request
     * @static
     * @return  object
     */
    private static function from_rest () {
      if (self::check_content_type(self::get_content_type(), ['plain', 'json']) || isset($_GET['HTTP_PAYLOAD_WRAPPED'])) {
        $request_method = 'No Request Method available';
        if (self::get_method() !== null) {
          $request_method = self::get_method();
        }
        $path_info = 'No Path Info available';
        if (self::get_path() !== null) {
          $path_info = self::get_path();
        }
        try {
          if (isset($_GET['HTTP_PAYLOAD_WRAPPED'])) {
            $input = $_GET['HTTP_PAYLOAD_WRAPPED'];
          }
          else {
            $input = file_get_contents(self::$inuri);
          }
          // Log::write(sprintf("<header>\n\t%s\n</header>", print_r($_COOKIE, true)));
          Log::write(sprintf("<input>\n\t%s / %s / %s\n</input>", $request_method, $path_info, $input));
          $data = json_decode($input);
          return $data;
        }
        catch (\Exception $e) {
          Log::write(sprintf("<input>\n\t%s / %s / %s\n</input>", $request_method, $path_info, "-- No Input --"));
          return null;
        }
      }
      return null;
    }

    /**
     * Get input
     * @static
     * @return  object
     */
    public static function input () {
      return self::from_rest();
    }
  }
?>
