<?php

	namespace greenscale\server\io;
	
	use greenscale\server\io\OutputAnswer;

	/**
	 * Class OutputFailedDependency models Generic answer for failed operations
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class OutputFailedDependency extends OutputAnswer {
		/**
		 * Options
		 * @var			object
		 */
		private $options = null;

		/**
		 * Payload
		 * @var			object
		 */
		private $payload = null;
		
		/**
		 * Constructor of OutputAnswer-type class initializes options
		 * @param			object $options Options, optional
		 * @param			object $payload Payload, optional
		 */
		function __construct($options = null, $payload = null) {
			$this->options = $options;
			$this->payload = $payload;
		}
		
		/**
		 * Generate REST answer with corresponding response code
		 * @return 		object
		 */
		public function to_rest() {
			return $this->to_rest_raw(424, $this->options, $this->payload);
		}
	}
?>
