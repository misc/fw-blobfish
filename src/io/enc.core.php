<?php
	
	namespace greenscale\server\io;
	
	/**
	 * @author			Christian Fraß <frass@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class Enc {
		
		/**
		 * @static
		 * @todo use mb_strtolower instead of strtolower
		 */
		public static function string_tolower($string) {
			if ($string === null) {
				return null;
			}
			else {
				// return mb_strtolower($string, "UTF-8");
				return strtolower($string);
			}
		}
		
		
		public static function encodeURIComponent($str) {
			$revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
			return strtr(rawurlencode($str), $revert);
		}

		/**
      * @static
			* function to enode 'complex' field of url
		  */
		public static function encode_complex($obj){
			return self::encodeURIComponent(
				base64_encode(
					json_encode($obj)
				)
			);	
		}
	}
	
 ?>
