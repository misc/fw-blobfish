<?php
	namespace greenscale\server\io;
	
	use greenscale\server\io\OutputAnswer;

	/**
	 * Class OutputConflict models Generic answer for failed operations
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class OutputConflict extends OutputAnswer {
		/**
		 * Options
		 * @var			object
		 */
		private $options = null;
		
		/**
		 * Constructor of OutputAnswer-type class initializes options
		 * @param			object $options Options, optional
		 */
		function __construct($options = null) {
			$this->options = $options;
		}

		/**
		 * Generate REST answer with corresponding response code
		 * @return 		object
		 */
		public function to_rest() {
			return $this->to_rest_raw(409, $this->options);
		}
	}
?>
