<?php
	namespace greenscale\server\service;

	/**
	 * Class ServiceException provides exception definition
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class ServiceException extends \Exception {
		/**
		 * Name of exception
		 * @var				string
		 */
		private $name;
		
		/**
		 * Constructor of class ServiceException sets name and message
		 * @param 		string $service_name Name of service
		 * @param			string $exception_name Name of exception
		 * @return  
		 */
		public function __construct ($service_name, $exception_name) {
			$this->name = $exception_name;
			$message = "Service ".$service_name." trapped ".$exception_name;
			parent::__construct($message, 0);
		}
		
		/**
		 * Get name of exception
		 * @return  	string
		 */
		public function getName () {
			return $this->name;
		}
	}
?>