<?php
  namespace greenscale\server\service;

  /**
   * Class Service provides abstraction for services
   * @author      Martin Springwald <springwald@greenscale.de>
   * @license     Greenscale Open Source License
   */
  abstract class Service {
    /**
     * Name of service, abstraction
     * @var       string
     */
    private $name = null;

    /**
     * Cache TTL
     */
    private $ttl;

    /**
     * Constructor of class Service sets name
     */
    public function __construct ($name) {
      $this->name = $name;
    }

    /**
     * Get name of service
     * @return    string
     */
    public function get_name () {
      return $this->name;
    }

    /**
     * Send cache headers
     */
    public function sendCacheHeaders ($ttl) {
      if ( headers_sent() ) {
        // Exception, this should not happen!
      }
      else {
        $cache_contol_realm = 'public';
        $current_timestamp = time();
        $current_time_interval_begin = intval(
          round( ( $current_timestamp / $ttl ) + 0, 0, PHP_ROUND_HALF_DOWN ) * $ttl
        );
        $current_time_interval_limit = intval(
          round( ( $current_timestamp / $ttl ) + 1, 0, PHP_ROUND_HALF_DOWN ) * $ttl
        );

        $current_time_interval_datestring = gmdate("D, d M Y H:i:s", $current_time_interval_begin);
        $expiration_datestring = gmdate("D, d M Y H:i:s", $current_time_interval_limit);

    	$payload_wrapper = $_GET['HTTP_PAYLOAD_WRAPPED'];
    	$hash_raw = [
    		$this->name,
    		'?',
    		$payload_wrapper,
    		strval($current_time_interval_limit)
    	];

        $data_hash = md5( // no other indicator for data changes atm
          implode('', $hash_raw) // implode is actually more optimized than join or "."
        );
        
        $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? intval($_SERVER['HTTP_IF_MODIFIED_SINCE']) : $current_timestamp;
        $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : null;
    
        $send_not_modified = (
          ( 
            ( $if_none_match === implode('', ['W/' , $data_hash] ) ) 
            || ($if_none_match === $data_hash) 
            || ($if_none_match === "\"" . $data_hash . "\"") 
          ) 
          && ($if_modified_since <= $current_time_interval_begin)
    	  && false // TODO: for debugging reasons
        );

	    session_cache_limiter(''); // disables automatic headers sent by session_start()

        header( implode( [ 'Etag: W/', $data_hash ]) );

        if ($send_not_modified) {
          header('HTTP/1.1 304 Not Modified', true, 304);
          exit();
        }

        header( implode( [ 'Expires: ' , $expiration_datestring, 'GMT'] ) );
        header( implode( [ 'Cache-Control: ' , $cache_contol_realm, ', max-age=', $ttl ] ) );
        header( implode( [ 'Last-Modified: ' , $current_time_interval_datestring, ' GMT'] ) );
      }
    }
  }
?>
