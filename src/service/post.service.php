<?php
  namespace greenscale\server\service;

  use greenscale\server\auth\ACLManager;
  use greenscale\server\Config;
  use greenscale\server\io\Input;
  use greenscale\server\io\Log;
  use greenscale\server\io\Output;
  use greenscale\server\io\OutputForbidden;
  use greenscale\server\router\Route;
  use greenscale\server\service\Service;

  /**
   * Class PostService provides abstraction for POST services
   * @author      Martin Springwald <springwald@greenscale.de>
   * @license     Greenscale Open Source License
   */
  abstract class PostService extends Service {
    /**
     * ACL manager
     * @var       \ACLManager
     */
    private $aclman;

    /**
     * Allow object
     * @var       object
     */
    protected $allow;

    /**
     * Denotes service which expects input data
     * @var       boolean
     */
    private $with_input;

    /**
     * Denotes service which uses session context
     * @var       boolean
     */
    private $with_session;

    /**
     * Constructor of class PostService
     * Install routes
     * @param   \Router $router Router
     * @param   string $name Name of service
     * @param   object $allow Allow-options, optional
     * @param   boolean $with_input Define if service uses input, defaults to true
     * @param   boolean $with_session Define if service uses session, defaults to true
     * @param   boolean $with_wrapper Define if service uses POST over GET wrapper, defaults to false
     * @param   integer $ttl Cache TTL, defaults to 500
     */
    function __construct ($router, $name, $allow = null, $with_input = true, $with_session = true, $with_wrapper = false, $ttl = 500) {
      parent::__construct($name);
      $this->aclman = ACLManager::get_instance();
      if ($allow === null) {
        $this->allow = (object) array(
          'origin' => Config::get()->main->allow_origin
        );
      }
      else {
        $this->allow = $allow;
      }
      $this->with_input = $with_input;
      $this->with_session = $with_session;
      $this->ttl = $ttl;
      if ($with_wrapper === true) {
        $router->add(new Route("GET", $name, $this->handleGetRequest()));
      }
      $router->add(new Route("POST", $name, $this->handlePostRequest()));
    }

    /**
     * Prepare response for error
     * @param   string $name
     * @return  object
     */
    public function prepareErrorResponse ($name) {
      $response = array(
        'payload' => array(
          'name' => $name
        )
      );
      return $response;
    }

    /**
     * Process post requests, abstraction
     * @param     object Input data (JSON)
     */
    abstract public function processPostRequest ($data);

    /**
     * Handle post request
     * @return    function
     */
    public function handlePostRequest () {
      return function () {
        $data = null;
        if ($this->with_input) {
          $data = Input::input();
        }
        if ($this->with_session) {
          session_start();
        }
        if ($this->aclman->check($this->get_name(), $data)) {
          Output::output($this->processPostRequest($data));
        }
        else {
          Output::output(new OutputForbidden($this->allow));
        }
      };
    }

    /**
     * Handle get request
     * @return    function
     */
    public function handleGetRequest () {
      $handler = $this->handlePostRequest();
      return function () use ($handler) {
        $this->sendCacheHeaders($this->ttl);
        $handler();
      };
    }
  }
?>
