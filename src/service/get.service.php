<?php
  namespace greenscale\server\service;

  use greenscale\server\auth\ACLManager;
  use greenscale\server\Config;
  use greenscale\server\io\Input;
  use greenscale\server\io\Log;
  use greenscale\server\io\Output;
  use greenscale\server\io\OutputForbidden;
  use greenscale\server\router\Route;
  use greenscale\server\service\Service;

  /**
   * Class GetService provides abstraction for GET services
   * @author      Martin Springwald <springwald@greenscale.de>
   * @license     Greenscale Open Source License
   */
  abstract class GetService extends Service {
    /**
     * ACL manager
     * @var       \ACLManager
     */
    private $aclman;

    /**
     * Allow object
     * @var       object
     */
    protected $allow;

    /**
     * Denotes service which uses session context
     * @var       boolean
     */
    private $with_session;

    /**
     * Constructor of class GetService
     * Install routes
     * @param   \Router $router Router
     * @param   string $name Name of service
     * @param   object $allow Allow-options, optional
     * @param   boolean $with_session Define if service uses session, defaults to true
     * @param   integer $ttl Cache TTL, defaults to 500
     */
    function __construct ($router, $name, $allow = null, $with_input = true, $with_session = true, $ttl = 500) {
      parent::__construct($name);
      $this->aclman = ACLManager::get_instance();
      if ($allow === null) {
        $this->allow = (object) array(
          'origin' => Config::get()->main->allow_origin
        );
      }
      else {
        $this->allow = $allow;
      }
      $this->with_input = $with_input;
      $this->with_session = $with_session;
      $this->ttl = $ttl;
      $router->add(new Route("GET", $name, $this->handleGetRequest()));
    }

    /**
     * Process get requests, abstraction
     */
    abstract public function processGetRequest ();

    /**
     * Handle get request
     * @return    function
     */
    public function handleGetRequest () {
      return function () {
        $this->sendCacheHeaders($this->ttl);
        $data = null;
        if ($this->with_input) {
          $data = Input::input();
        }
        if ($this->with_session) {
          session_start();
        }
        if ($this->aclman->check($this->get_name(), $data)) {
          Output::output($this->processGetRequest($data));
        }
        else {
          Output::output(new OutputForbidden($this->allow));
        }
      };
    }
  }
?>
