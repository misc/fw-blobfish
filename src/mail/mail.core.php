<?php
	namespace greenscale\server\mail;

	use greenscale\server\Config;
	use greenscale\server\io\Log;
	use greenscale\server\mail\MailTemplate;
    use Symfony\Component\Mime\Email;
	use Symfony\Component\Mime\Part\DataPart;
    use Symfony\Component\Mime\Message;
    use Symfony\Component\Mime\Exception\ExceptionInterface;
    use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
    use Symfony\Component\Mailer\Mailer;
    use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
    
	/**
	 * Class MailManager allows to prepare and send emails
	 * @author			Martin Springwald <springwald@greenscale.de>
	 * @license			Greenscale Open Source License
	 */
	class MailManager {
		/**
		 * Email address of sender
		 * @var			string
		 */
		private $from = null;
		
		/**
		 * Underlying mailer instance
		 * @var			\Swift_Mailer
		 */
		private $mailer = null;
		
		/**
		 * Constructor of class MailManager reads profile from config and creates mailer instance
		 */
		function __construct () {
			$mail_profile = Config::get()->mail;
			$this->from = $mail_profile->from;
			$transport =
				new EsmtpTransport(
					$mail_profile->smtp_host,
					$mail_profile->smtp_port,
					$mail_profile->smtp_tunnel === "tls" ? false : $mail_profile->smtp_tunnel
				);
			$this->mailer = new Mailer(
				$transport
					->setUsername($mail_profile->username)
					->setPassword($mail_profile->password)
			);
		}
		
		/**
		 * Send email message without frills
		 * @param		string $subject Subject of email
		 * @param 	string $to Email address of recipient
		 * @param		string $body Body of email
		 */
		public function sendSimpleMessage ($subject, $to, $body) {
    		$debug_mail = isset(Config::get()->main->debug_log_mail) && Config::get()->main->debug_log_mail ? true : false;
			try {
				$message_ = new Email();
				$message = $message_->subject($subject)->from($this->from)->to($to)->text($body);
				$result = @$this->mailer->send($message);
				return $result;
			}
			catch (TransportExceptionInterface $exception) {
				Log::write(sprintf("<exception>\n\t%s:\n\t%s\n</exception>", "MailManager", $exception->getMessage()), $debug_mail);
				return 0;
			}
			catch (ExceptionInterface $exception) {
				Log::write(sprintf("<exception>\n\t%s:\n\t%s\n</exception>", "MailManager", $exception->getMessage()), $debug_mail);
				return 0;
			}
		}
		
		/**
		 * Create Attachement
		 * @param		string $path Path to file
		 * @param 	    string $filename Embedded filename
		 * @returns
		 */
		public function createAttachement ($path, $filename = null, $content_type = null) {
		    return DataPart::fromPath($path, $filename, $content_type);
		}

		/**
		 * Send email message from template
		 * @param		string $subject Subject of email
		 * @param 	string $to Email address of recipient
		 * @param		string $template Name of template
		 * @param		string $data Data to apply
		 * @param		string $embeddings Embeddings to apply
		 */
		public function sendTemplateMessage ($subject, $to, $template_name, $data, $embeddings = null) {
    		$debug_mail = isset(Config::get()->main->debug_log_mail) && Config::get()->main->debug_log_mail ? true : false;
			try {
				$message = new Email();
				$message->subject($subject)
				        ->from($this->from)
				        ->to($to);
				$template = new MailTemplate($template_name);
				if ($embeddings != null) {
 		    	  foreach($embeddings as $embedding) {
 		    	      $cid  = $embedding->asInline()->getContentId();
 		    	      $message->addPart($embedding->asInline());
				      $data->{"cid.".$embedding->getContentId()} = "cid:" . $cid;
				      $data->{"cid.".$embedding->getFilename()} = "cid:" . $cid;
				  }
				}
				$processed = $template->process_html($data);
				$message->html($processed);
				$plain = $template->process_plain($data);
				$message->text($plain);
				$result = @$this->mailer->send($message);
				Log::write(sprintf("<mail>\n\t%s:\n\t%s\n\t%s\n\n\t%s\n</mail>", "MailManager", $to, strval($result), $plain), $debug_mail);
				return $result;
			}
			catch (TransportExceptionInterface $exception) {
				Log::write(sprintf("<exception>\n\t%s:\n\t%s\n</exception>", "MailManager", $exception->getMessage()), $debug_mail);
				return 0;
			}
			catch (ExceptionInterface $exception) {
				Log::write(sprintf("<exception>\n\t%s:\n\t%s\n</exception>", "MailManager", $exception->getMessage()), $debug_mail);
				return 0;
			}
		}

		/**
		 * Get instance
		 * @return		\MailManager
		 * @todo optimization?
		 */
		 public static function get_instance () {
			return new MailManager();
		}
	}
?>
