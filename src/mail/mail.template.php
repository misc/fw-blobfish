<?php
	namespace greenscale\server\mail;

	use greenscale\server\Config;
	use greenscale\server\io\Template;

	/**
	 * Class MailTemplate	process email templates
	 * @author				Martin Springwald <springwald@greenscale.de>
	 * @license				Greenscale Open Source License
	 */
	class MailTemplate {
		/**
		 * Html Content Template
		 * @var				string
		 */
		private $html_template = null;

		/**
		 * Plain Content Template
		 * @var				string
		 */
		private $plain_template = null;
		
		/**
		 * Template Engine
		 * @var				\Template
		 */
		private $template_engine = null;
		
		/**
		 * Constructor of class MailTemplate makes template ready from file to be processed
		 * @param			string $name Name of template
		 */
		public function __construct($name) {
			$this->load_html_template_from_file($name);
			$this->load_plain_template_from_file($name);
			$this->template_engine = new Template();
		}
		
		/**
		 * Translate
		 * @param 		string $result 
		 * @return  		string
		 */
		public static function translate ($result) {
			return Template::translate($result);
		}
		
		/**
		 * Process template and apply data for HTML
		 * @param			$data Data to apply
		 * @return			string
		 */
		public function process_html ($data) {
			return $this->template_engine->process($this->html_template, $data);
		}
		
		/**
		 * Process template and apply data for Plain
		 * @param			$data Data to apply
		 * @return			string
		 */
		public function process_plain ($data) {
			return $this->template_engine->process($this->plain_template, $data);
		}

		/**
		 * Load contents from file
		 * @param			$name Name of template
		 */
		private function load_html_template_from_file ($name) {
			$this->html_template = Template::load_html_template_from_file($name);
		}

		/**
		 * Load contents from file
		 * @param			$name Name of template
		 */
		private function load_plain_template_from_file ($name) {
			$this->plain_template = Template::load_plain_template_from_file($name);
		}
	}
?>
